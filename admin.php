<?php

function wkcalendar_acf_save_post($post_id) {

	if( have_rows('wkc_booked_dates', $post_id) ) {
		$i = 0;
		while( have_rows('wkc_booked_dates', $post_id) ) {
			$i++;
			$row = the_row();
			if($row['field_wkc_booking_conf'] == 1 && $row['field_wkc_booking_conf_conf'] == 0){
				$row['field_wkc_booking_conf_conf'] = 1;

				$to = $row['field_wkc_booked_dates_email'];
				$subject = 'Booking confirmation';
				$body = '';
				$body .= "Hi, I just want to notify you that your appointment with us at:" . "\n";
				$body .= $row['field_wkc_booked_dates_time']  . " \n";
				$body .= "has been confirmed by our admin.";

				wp_mail( $to, $subject, $body );

				update_row('wkc_booked_dates', $i, $row, $post_id);
			}
		}
	}
}

add_action('acf/save_post', 'wkcalendar_acf_save_post', 20);

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(
    array(
			'page_title' 	=> 'Calendar',
			'menu_title'	=> 'Calendar',
			'menu_slug' 	=> 'wkcalendar',
			'icon_url' 		=> 'dashicons-calendar-alt',
			'redirect'		=> true,
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' 	=> 'Settings',
			'menu_title'	=> 'Settings',
			'parent_slug'	=> 'wkcalendar',
			'menu_slug' 	=> 'wkcalendar-settings',
			'capability'	=> 'edit_posts',
		)
	);

	acf_add_options_sub_page(
    array(
			'page_title' 	=> 'Time Slots',
			'menu_title'	=> 'Time Slots',
			'parent_slug'	=> 'wkcalendar-settings',
			'menu_slug' 	=> 'wkcalendar-time-slots',
			'capability'	=> 'edit_posts',
		)
	);

	acf_add_options_sub_page(
    array(
			'page_title' 	=> 'Bookings',
			'menu_title'	=> 'Bookings',
			'parent_slug'	=> 'wkcalendar-settings',
			'menu_slug' 	=> 'wkcalendar-bookings',
			'capability'	=> 'edit_posts',
		)
	);

}

if( function_exists('acf_add_local_field_group') ){
	// BOOKINGS
	acf_add_local_field_group(
    array (
			'key' => 'group_wkcalendar_bookings',
			'title' => 'Bookings',
			'fields' => array (
				array (
					'key' => 'field_wkc_booked_dates',
					'label' => 'Booked Time Slots',
					'name' => 'wkc_booked_dates',
					'type' => 'repeater',
					'instructions' => 'The following time slots have been booked.',
					'layout' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_wkc_booked_dates_name',
							'label' => 'Name',
							'name' => 'name',
							'type' => 'text',
							'wrapper' => array (
								'width' => '22',
							),
						),
						array (
							'key' => 'field_wkc_booked_dates_email',
							'label' => 'Email',
							'name' => 'email',
							'type' => 'email',
							'wrapper' => array (
								'width' => '22',
							),
						),
						array (
							'key' => 'field_wkc_booked_dates_website',
							'label' => 'Website',
							'name' => 'website',
							'type' => 'text',
							'wrapper' => array (
								'width' => '22',
							),
						),
						array (
							'key' => 'field_wkc_booked_dates_time',
							'label' => 'Time',
							'name' => 'time',
							'type' => 'text',
							'wrapper' => array (
								'width' => '22',
							),
						),
						array (
							'ui' => 1,
							'default_value' => 0,
							'key' => 'field_wkc_booking_conf',
							'label' => 'Confirm',
							'type' => 'true_false',
							'name' => 'confirm',
							'wrapper' => array (
								'width' => '12'
							)
						),
						array (
							'default_value' => 0,
							'key' => 'field_wkc_booking_conf_conf',
							'label' => 'Confirm Confirmation',
							'type' => 'true_false',
							'name' => 'conf_confirm',
							'wrapper' => array (
								'width' => '0',
								'class' => 'acf-hidden',
							)
						),
						array (
							'key' => 'field_wkc_booked_dates_start_time',
							'label' => 'Start Time',
							'name' => 'start_time',
							'type' => 'date_time_picker',
							'display_format' => 'c',
							'return_format' => 'c',
							'first_day' => 1,
							'wrapper' => array (
								'width' => '0',
								'class' => 'acf-hidden',
							),
						),
						array (
							'key' => 'field_wkc_booked_dates_end_time',
							'label' => 'End Time',
							'name' => 'end_time',
							'type' => 'date_time_picker',
							'display_format' => 'c',
							'return_format' => 'c',
							'first_day' => 1,
							'wrapper' => array (
								'width' => '0',
								'class' => 'acf-hidden',
							),
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'wkcalendar-bookings',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'active' => 1,
		)
	);

	// TIME SLOTS
	acf_add_local_field_group(
    array (
			'key' => 'group_wkcalendar_time_slots',
			'title' => 'Weekly Calendar',
			'fields' => array (
				array (
					'key' => 'field_wkc_start_week',
					'label' => 'Starting Week',
					'name' => 'wkc_start_week',
					'type' => 'date_picker',
					'instructions' => 'Please select the starting week.',
					'display_format' => 'F j, Y',
					'return_format' => 'Y-m-d',
					'first_day' => 1,
					'required' => 1,
				),
				array (
					'key' => 'field_wkc_your_week',
					'label' => 'Your Weeks',
					'name' => 'wkc_your_week',
					'type' => 'repeater',
					'instructions' => 'Please select your available time slots.',
					'layout' => 'table',
					'button_label' => 'Add New Week',
					'sub_fields' => array (
						array (
							'key' => 'field_wkc_time_slots',
							'label' => 'Time Slots',
							'name' => 'time_slots',
							'type' => 'repeater',
							'layout' => 'table',
							'button_label' => 'Add New Time Slot',
							'collapsed' => 'field_show_on_weeks',
							'sub_fields' => array (
								array (
									'display_format' => 'g:i a',
									'return_format' => 'g:i a',
									'key' => 'field_wkc_time_slots_start_time',
									'label' => 'Start Time',
									'name' => 'start_time',
									'type' => 'time_picker',
									'wrapper' => array (
										'width' => '20',
									),
								),
								array (
									'key' => 'field_wkc_time_slots_end_time',
									'label' => 'End Time',
									'name' => 'end_time',
									'type' => 'time_picker',
									'display_format' => 'g:i a',
									'return_format' => 'g:i a',
									'wrapper' => array (
										'width' => '20',
									),
								),
								array (
									'key' => 'field_wkc_time_slots_show_on_weeks',
									'label' => 'Days',
									'name' => 'show_on_weeks',
									'type' => 'checkbox',
									'layout' => 'horizontal',
									'choices' => array (
										'Monday' => 'Monday',
										'Tuesday' => 'Tuesday',
										'Wednesday' => 'Wednesday',
										'Thursday' => 'Thursday',
										'Friday' => 'Friday',
										'Saturday' => 'Saturday',
										'Sunday' => 'Sunday',
									),
									'default_value' => array (
										0 => 'Monday',
										1 => 'Tuesday',
										2 => 'Wednesday',
										3 => 'Thursday',
										4 => 'Friday',
										5 => 'Saturday',
										6 => 'Sunday',
									),
									'return_format' => 'label',
								),
							),
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'wkcalendar-time-slots',
					),
				),
			),
			'menu_order' => 0,
		)
	);

	// SETTINGS
	acf_add_local_field_group(
		// Email Settings
    array (
			'key' => 'group_wkcalendar_email_settings',
			'title' => 'Email Settings',
			'fields' => array (
				array (
					'key' => 'field_wkc_admin_email',
					'label' => 'Notification Email',
					'name' => 'wkc_admin_email',
					'type' => 'email',
					'instructions' => 'Please select the email that will be recieving the notifications (if empty notifications will go to the admin email).'
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'wkcalendar-settings',
					),
				),
			),
			'menu_order' => 0,
		)
	);
	acf_add_local_field_group(
		// Google Recaptcha
    array (
			'key' => 'group_wkcalendar_google_recaptcha',
			'title' => 'Google Recaptcha',
			'fields' => array (
				array (
					'key' => 'field_wkc_recaptcha_key',
					'label' => 'Recaptcha Key',
					'name' => 'wkc_recaptcha_key',
					'type' => 'text',
				),
				array (
					'key' => 'field_wkc_recaptcha_secret',
					'label' => 'Recaptcha Secret',
					'name' => 'wkc_recaptcha_secret',
					'type' => 'text',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'wkcalendar-settings',
					),
				),
			),
			'menu_order' => 1,
		)
  );
	acf_add_local_field_group(
    // Styling Settings
    array (
			'key' => 'group_wkcalendar_styling',
			'title' => 'Styling Settings',
			'fields' => array (
				array (
					'key' => 'field_wkc_active_fa',
					'label' => 'Enable FontAwesome',
					'name' => 'wkc_active_fa',
					'type' => 'true_false',
					'instructions' => 'If FontAwesome isn\'t already enabled please tick this option.',
				),
				array (
					'key' => 'field_wkc_primary_color',
					'label' => 'Primary Color',
					'name' => 'wkc_primary_color',
					'type' => 'color_picker',
					'instructions' => 'Please select your primary color (optional).',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'wkcalendar-settings',
					),
				),
			),
			'menu_order' => 2,
		)
  );
}