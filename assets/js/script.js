(function($) {

  $(document).ready(function() {

    $('.wkc-popup-wrapper').hide();

    $('.wkc-week').not(':eq(0)').fadeOut(200);

    if($('.wkc-week').length <= 1){
      $('.wkc-weekly-nav').hide();
    }

    $('td.wkc-timeslot .wkc-time').click(function(){
      slotClicked(this);
    });

    $('#wkc_next').click(function(){
      fadeNext();
    });
    $('#wkc_prev').click(function(){
      fadePrev();
    });

    $('#wkc_popup_open').click(function(){
      $('.wkc-popup-wrapper').fadeIn(200);
    });

    $('.wkc-popup-close').click(function(){
      $('.wkc-popup-wrapper').fadeOut(200);
    });

    function fadeNext(){
      $('.wkc-body-inner .wkc-week:visible').eq(0).fadeOut(200,function(){
        $(this).next('.wkc-week').fadeIn(500);
        $(this).appendTo('.wkc-body-inner');
      });
    }

    function fadePrev(){
      $('.wkc-body-inner .wkc-week:visible').eq(0).fadeOut(200,function(){
        $('.wkc-body-inner .wkc-week:last').fadeIn(500).prependTo('.wkc-body-inner');
      });
    }

    function slotClicked(selector){
      selector = $(selector).parent();
      if($(selector).text() != ''){
        $('.is-selected').removeClass('is-selected');
        $('.wkc-popup').addClass('timeslot-selected');
        $(selector).addClass('is-selected');
        $('#startET').val($(selector).attr('data-event-start'));
        $('#endET').val($(selector).attr('data-event-end'));
        $('.wkc-popup-time').html($(selector).html());
        $('.wkc-popup-date').html($(selector).attr('data-weekday'));
      }
    }

  });

})( jQuery );