<?php

function wkcalendar_enqueue_scripts() {
	// Plugin stylesheet.
	wp_enqueue_style( 'wkc-style', wkcalendar_plugin_url( 'assets/css/style.css' ), array(), null );
	// Plugin javascript.
	wp_enqueue_script( 'wkc-js', wkcalendar_plugin_url( 'assets/js/script.js' ), array( 'jquery' ), '1.0.0', true );

	$fontawesome = get_field('wkc_active_fa', 'options');

	if($fontawesome){
		wp_enqueue_script( 'wkc-js', 'https://use.fontawesome.com/1de8049edf.js', array(), '4.3.3', true );
	}
}

function wkcalendar_color_settings(){

	$color = get_field('wkc_primary_color', 'options');

	$output = '';

	$output .= '
<style type="text/css">
	.btn-wkc {
		background-color:' . $color . ' !important;
		border-color: #373a3c !important;
		color: #fff !important;
		-webkit-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
	}
	.btn-wkc:hover,
	.btn-wkc:focus {
		background-color: #373a3c !important;
		border-color: #373a3c !important;
		color: #fff !important;
	}
	.wkc-week-header{
		border-bottom-color:' . $color . ';
		background-color:' . $color . ';
	}
	.wkc-timeslot.is-selected .wkc-time{
		background-color:' . $color . ';
	}
</style>
';

	if($color){
		echo $output;
	}

}
add_action('wp_head', 'wkcalendar_color_settings');
