<?php

function wkcalendar_shortcode(){

	$output = '';
	$weeks = get_field('wkc_your_week', 'options');
	$start_week = get_field('wkc_start_week', 'options');
	$booked_dates = get_field('wkc_booked_dates', 'options');

	$output .= '
	<div class="wkcalendar" id="wkcalendar">';

	if($weeks){

		$selectedDate = date('c' , strtotime('monday this week' . $start_week));
		$weekd = 5;
		$weeknr = 0;

		$output .= '
			<div class="wkc-header">
				<nav class="wkc-weekly-nav">
					<span class="wkc-nav-btn wkc-nav-prev" id="wkc_prev"> &lt; Previous Week</span>
					<span class="wkc-nav-btn wkc-nav-next" id="wkc_next">Next Week &gt; </span>
				</nav>
			</div>
			<div class="wkc-body">
				<div class="wkc-body-inner">';

		foreach($weeks as $week){

			$output .= '
			<div class="wkc-week">
				<div class="wkc-week-header">' . date('F Y' , strtotime($selectedDate)) . '</div>
				<table class="wkc-week-body">';

			if($week){

				$time_slots = $week['time_slots'];

				if($time_slots) {

					foreach($time_slots as $time_slot)

						for($i=0; $i<sizeof($time_slot['show_on_weeks']); $i++)
							if($time_slot['show_on_weeks'][$i] == "Saturday" || $time_slot['show_on_weeks'][$i] == "Sunday")
								$weekd = 7;

					$output .= '
						<tr>';

					for($i=0; $i<$weekd; $i++) {

						$plh = strtotime($selectedDate . "+" . $i . ' days');
						$output .= '
							<th';

						if(date('Y-m-d') == date('Y-m-d', $plh)) $output .= ' class="today"';

						$output .='>' . date('D', $plh) . " " . date('jS', $plh) . '</th>';
					}

					$output .= '
						</tr>';

					foreach($time_slots as $time_slot){

						if($time_slot){

							$output .= '
								<tr>';

							for($i=0; $i<$weekd; $i++) {

							 	$day = date('Y-m-d', strtotime($selectedDate . "+" . $i . ' days'));
							 	$dayOfWeek = date('l', strtotime($selectedDate . "+" . $i . ' days'));
							 	$dayNr = date('dS', strtotime($selectedDate . "+" . $i . ' days'));
							 	$eventStart = date('c', strtotime($day . $time_slot['start_time']));
							 	$eventEnd = date('c', strtotime($day . $time_slot['end_time']));

								$output .= '
									<td class="wkc-timeslot" data-weekday="' . $dayOfWeek . ' ' . $dayNr . '" data-event-start="' . $eventStart . '" data-event-end="' . $eventEnd . '">';
							 	for($y=0; $y<$weekd; $y++){

							 		$taken = false;

							 		if($booked_dates!=null)

							 			for($row=0; $row<sizeof($booked_dates); $row++)

							 				if($eventStart == $booked_dates[$row]['start_time'] && $eventEnd == $booked_dates[$row]['end_time'])
							 					$taken = true;

						 			if(isset($time_slot['show_on_weeks'][$y]) && $dayOfWeek == $time_slot['show_on_weeks'][$y] && !$taken)

						 				$output .= '
										 	<div class="wkc-time">
									 			<span class="wkc-time-start">' . $time_slot['start_time'] . '</span>
									 			<span class="wkc-time-divider"> - </span>
									 			<span class="wkc-time-end">' . $time_slot['end_time'] . '</span>
									 		</div>';
							 	}
							 	$output .= '
									</td>';
							}
							$output .= '
								</tr>';
						}
					}

				}

				$output .= '
					</table>
				</div>';
				$selectedDate = date('Y-m-d' , strtotime('+1'  . ' week' . $selectedDate));
			}
		}

		$output .= '
			</div>
		</div>';

	}

	$output .= '
		<div class="wkc-footer">
			<button class="btn btn-1 btn-wkc" id="wkc_popup_open">Book Call</button>
		</div>';

	$output .= '
		</div>';
	return $output;


}
add_shortcode('wkcalendar', 'wkcalendar_shortcode');


function wkcalendar_popup(){

	$recaptcha_sitekey = get_option('wkc_recaptcha_key');

	$output = '

	<div class="wkc-popup-wrapper" id="wkc_popup" style="display:none">
		<div class="wkc-popup">
			<span class="wkc-popup-close"></span>
			<div class="wkc-popup-inner">
				<div class="wkc-popup-warning">Are you sure you want to book for this time?</div>
				<div class="wkc-popup-confirm-time">
					<div class="wkc-popup-date">Please select a time slot.</div>
					<div class="wkc-popup-time"></div>
				</div>
				<div class="wkc-popup-text">
					<div class="wkc-popup-title">Complete the form and confirm your time.</div>
					<form id="wkc_booker" method="POST">
						<fieldset class="form-group">
							<label for="clientName">Name:</label>
							<input class="form-control" id="clientName" type="name" name="wkcalendar-name" placeholder="Enter your name..." required/>
						</fieldset>
						<fieldset class="form-group">
							<label for="clientEmail">Email:</label>
							<input class="form-control" id="clientEmail" type="email" name="wkcalendar-email" placeholder="Enter your email..." required/>
						</fieldset>
						<fieldset class="form-group">
							<label for="clientWebsite">Website:</label>
							<input class="form-control" id="clientWebsite" type="website" name="wkcalendar-website" placeholder="Enter your website..." required/>
						</fieldset>
						<input id="startET" name="wkcalendar-starttime" type="hidden"/>
						<input id="endET" name="wkcalendar-endtime" type="hidden"/>';

	if($recaptcha_sitekey) {
		$output .= '
						<div class="g-recaptcha" data-sitekey="' . $recaptcha_sitekey . '"></div>
';
	}
	$output .= '
						<input class="btn-1 btn-wkc" id="clientSubmit" data-callback="wkc_onSubmit" name="wk-submit" type="submit" value="Confirm Booking"/>
					</form>
				</div>
			</div>
		</div>
	</div>
';

	echo $output;

}
add_action('wp_footer', 'wkcalendar_popup');


function wkcalendar_update(){

	if (isset($_POST["wkcalendar-starttime"])) {
		$stime = $_POST['wkcalendar-starttime'];
	} else {
		$stime = null;
	}

	if (isset($_POST["wkcalendar-endtime"])) {
		$etime = $_POST['wkcalendar-endtime'];
	} else {
		$etime = null;
	}

	if (isset($_POST["wkcalendar-name"])) {
		$wkcalendar_name = $_POST['wkcalendar-name'];
	} else {
		$wkcalendar_name = null;
	}

	if (isset($_POST["wkcalendar-email"])) {
		$wkcalendar_email = $_POST['wkcalendar-email'];
	} else {
		$wkcalendar_email = null;
	}

	if (isset($_POST["wkcalendar-website"])) {
		$wkcalendar_website = $_POST['wkcalendar-website'];
	} else {
		$wkcalendar_website = null;
	}

	$sdate = date('Y-m-d h:i a', strtotime($stime));
	$edate = date('h:i a', strtotime($etime));
	$event = array(
		'name' => $wkcalendar_name,
		'email' => $wkcalendar_email,
		'website' => $wkcalendar_website,
		'time' => $sdate . ' - ' . $edate,
		'start_time' => $stime,
		'end_time' => $etime
	);


	if(isset($_POST['wk-submit']) ){

		$recaptcha_secret_key = get_option('wkc_recaptcha_secret');

		$to = get_field('wkc_admin_email', 'options');
		if(!$to){
			$to = get_option( 'admin_email' );
		}
		$subject = 'New Booking';
		$body = '';
		$body .= "You have received a new booking. \n";
		$body .= "Name: " . $wkcalendar_name  . " \n";
		$body .= "Email: " . $wkcalendar_email  . " \n";
		$body .= "Website: " . $wkcalendar_website  . " \n";
		$body .= "Date: " . $sdate . " - " . $edate;

		wp_mail( $to, $subject, $body );

		$row_id = add_row('field_wkc_booked_dates', $event, 'options');
	}
}

add_action( 'init', 'wkcalendar_update' );


function wpcalendar_grecaptcha(){
	$output = '<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
    	function onSubmit(token) {
        	document.getElementById("wkc_booker").submit();
       }
     </script>';

     echo $output;
}

add_action( 'wp_enqueue_scripts', 'wkcalendar_enqueue_scripts', 98 );

add_action('wp_head', 'wpcalendar_grecaptcha');

