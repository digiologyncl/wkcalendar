<?php
/*
* Plugin Name: WKCalendar
* Description: A weekly calendar that uses the Wordpress database to store and edit events.
* Version: 2.2.9
* Author: Paul S
* Text Domain: wkcalendar
* License:
*/

define( 'WKCALENDAR_VERSION', '2.2.9' );

define( 'WKCALENDAR_REQUIRED_WP_VERSION', '4.7' );

define( 'WKCALENDAR_PLUGIN', __FILE__ );

define( 'WKCALENDAR_PLUGIN_BASENAME', plugin_basename( WKCALENDAR_PLUGIN ) );

define( 'WKCALENDAR_PLUGIN_NAME', trim( dirname( WKCALENDAR_PLUGIN_BASENAME ), '/' ) );

define( 'WKCALENDAR_PLUGIN_DIR', untrailingslashit( dirname( WKCALENDAR_PLUGIN ) ) );

// ACFPro Required
require_once ABSPATH . '/wp-content/plugins/advanced-custom-fields-pro/acf.php';

// ENQUEUE
require_once WKCALENDAR_PLUGIN_DIR . '/enqueue.php';

// ADMIN
require_once WKCALENDAR_PLUGIN_DIR . '/admin.php';

// PUBLIC
require_once WKCALENDAR_PLUGIN_DIR . '/public.php';

// PLUGIN URL
function wkcalendar_plugin_url( $path = '' ) {
  $url = plugins_url( $path, WKCALENDAR_PLUGIN );
  if ( is_ssl() && 'http:' == substr( $url, 0, 5 ) ) {
    $url = 'https:' . substr( $url, 5 );
  }
  return $url;
}